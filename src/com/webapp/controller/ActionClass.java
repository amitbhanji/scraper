package com.webapp.controller;

import java.io.InputStream;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ActionClass
{

	 public synchronized static HashMap buildActionClassMapping(InputStream inputStreamObj)
		{
		    DocumentBuilderFactory factory = null;
	        DocumentBuilder builder = null;
	        Document doc = null;
	        HashMap  map = null; 
	        
	        AppconfigBean configBeanObj = null;
	        try
			{

	            factory = DocumentBuilderFactory.newInstance();
	            builder = factory.newDocumentBuilder();
				
				doc = builder.parse(inputStreamObj);    	
				Element docEle = doc.getDocumentElement();
				NodeList nList = docEle.getChildNodes();            
				map = new HashMap();
			//	configBeanObj = new AppconfigBean();
	            for(int i=0; i<nList.getLength(); i++)
				{
					String temphandlerclass ="";
					String attrNodeValue = "";
					String attrNodeName = "";
					String uiActionName = "";
					String 	successPageVal = "";
					Node childNode = nList.item(i);
					if(childNode.getNodeName().equals("actionclass"))
					{
						NamedNodeMap attrs = childNode.getAttributes();
						if(attrs != null && attrs.getLength() > 0)
						{
							for(int iCount = 0 ; iCount < (attrs.getLength()) ; iCount++)
							{
								Node attrNode = attrs.item(iCount);
								if(attrNode != null)
								{
									attrNodeName = attrNode.getNodeName();								
									attrNodeValue = attrNode.getNodeValue();
								}
							}
						}
					}
			
					NodeList nList1 = childNode.getChildNodes();
					for(int j=0;j<nList1.getLength();j++)
					{
						Node childNode1 = nList1.item(j);					
						NodeList nList2 = childNode1.getChildNodes();
						for(int k=0; k<nList2.getLength(); k++)
						{
							Node childNode2 = nList2.item(k);

							NodeList nList3 = childNode2.getChildNodes();
							for(int l=0;l<nList3.getLength();l++)
							{
								Node childNode3 = nList3.item(l);
								if(childNode3.getNodeName().equals("name"))
								{
									System.out.println("ACTION Class NAME=====" + attrNodeValue);
									uiActionName = childNode3.getFirstChild().getNodeValue();
									System.out.println("*******In Action Class  uiActionName=====" + uiActionName);
									configBeanObj = new AppconfigBean();
									configBeanObj.setUiActionName(uiActionName);
									configBeanObj.setHandlerClassName(attrNodeValue);
									temphandlerclass	=	configBeanObj.getHandlerClassName();
									System.out.println("getting Handlerclass name temphandlerclass" + temphandlerclass);

									//System.out.println("*******In Action Class  HandlerClassName=====" + attrNodeValue);

								}
							else if(childNode3.getNodeName().equals("path"))
								{
									successPageVal = childNode3.getFirstChild().getNodeValue();
									//System.out.println("Success Page=====" + successPageVal);								
									configBeanObj.setUiSuccessPage(successPageVal);
								}
							//	System.out.println("Before put configBeanObj " + configBeanObj);
								map.put(uiActionName,configBeanObj);
							//	System.out.println("After put configBeanObj " + configBeanObj);
								
							}
						}
					}
				}			
	        } 
	        catch(Exception e)
	        {
	        	e.printStackTrace();
	        }
	        System.out.println("map object is" +map );
	        return map;
		
		}
		



















}
