package com.webapp.controller;

import javax.servlet.http.HttpServletRequest;

 

	public class GenericHandler 

	{

		public static String getRequestParameters(HttpServletRequest request, String parameterName) throws Exception
		{
			
			String paramValue = "";
			
			try
			{
			
				if(request.getParameter(parameterName) != null && request.getParameter(parameterName).length() > 0)
				{
					paramValue = request.getParameter(parameterName);
				}
		}
			catch(Exception e)
			{
			 e.printStackTrace();	
			}
			
			return paramValue;
			
		}
		public static void setRequestAttribute(HttpServletRequest request, String attributeName, Object attributeValue) throws Exception
		{
			try
			{
				 
				 
					request.setAttribute(attributeName, attributeValue);				
				 
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public static Object getRequestAttribute(HttpServletRequest request, String attributeName) throws Exception
		{
			Object obj = null;
			try
			{
				 
			 
					if(request.getAttribute(attributeName) != null)
					{
						obj = (Object)request.getAttribute(attributeName);
					}
				 
			}
			catch(Exception ex)
			{
				throw ex;
			}
			return obj;
		}












	
	
	
	
	
	
	
	}
