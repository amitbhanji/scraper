package com.webapp.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.webapp.viewhelper.GenericViewHelper;

public class ApplicationControllerServlet extends HttpServlet
{
	private static HashMap hmActionClassMap;

	/*  1. Init 
	 * 	2. service
	 *  3. destroy
	 *
	 */
	public void init(ServletConfig config) throws ServletException {
		InputStream inputStream = null;
		try 
		{
			super.init(config);
			ApplicationConstants.loadConstants();
			String path = ApplicationConstants.APP_CONFIG_XML_PATH;
			System.out.println("The Path is ====" + path);
			
			inputStream = (config.getServletContext()).getResourceAsStream(path);
			
			System.out.println("InputStreamObject is=====" + inputStream);
			hmActionClassMap = ActionClass.buildActionClassMapping(inputStream);
			System.out.println("The hmActionClassMap  is ===="+ hmActionClassMap);
			System.out.println("The init method ends");
			// ApplicationConstant.loadConstants();

		} catch (Exception e) 
		{
				e.printStackTrace();
		}
	}
	
	public void service(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException 
	{
		try {
			System.out.println("inside appcontrl service methode === ");
			process(request,response);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	private void process(HttpServletRequest request, HttpServletResponse response) {
		String here = "";
		String uiActionName = "";
		String sResultPage = "";
		String fwdString = "";
		AppconfigBean configBeanObj = null;
		
		if (request.getParameter("uiActionName") != null) 
		{
			uiActionName = request.getParameter("uiActionName");
			System.out.println("From Browser :: process():: uiActionName =="+ uiActionName);
		} 
		else 
		{
			// Default UIACTION NAME of the First Page of Application
			uiActionName = "doLogin";
		}
		
		try 
		{
			System.out.println("********INSIDE PROCESS METHOS**********");
			configBeanObj = new AppconfigBean();
			configBeanObj = (AppconfigBean) hmActionClassMap.get(uiActionName);
			String handlerClassName = configBeanObj.getHandlerClassName();
			Class cls = Class.forName(handlerClassName);
			GenericViewHelper viewHelperObj = (GenericViewHelper) cls.newInstance();
			sResultPage = viewHelperObj.execute(request, uiActionName);
			if (sResultPage != null && sResultPage.length() > 0) 
			{
				uiActionName = sResultPage;
			}
			AppconfigBean configObj = (AppconfigBean) hmActionClassMap.get(uiActionName);
			fwdString = configObj.getUiSuccessPage();
			System.out.println("fwdString === " + fwdString);
			try 
			{
				forwardRequest(request, response, fwdString);
			}
			catch (Exception ex) 
			{
			ex.printStackTrace();
			}
			uiActionName = null;

		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}

	}
	
	private void forwardRequest(HttpServletRequest request,HttpServletResponse response, String fwdString) throws Exception 
	{
		try 
	{
			RequestDispatcher rdObj = request.getRequestDispatcher(fwdString);
			rdObj.forward(request, response);
	}
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}
	}


}
