package com.webapp.controller;

public class AppconfigBean implements java.io.Serializable
{
	private String uiActionName;
	private String handlerClassName;
	private String uiSuccessPage; 

	public AppconfigBean()
	{
	
	}

	public void setUiActionName(String uiActionName)
	{
		this.uiActionName = uiActionName;
		System.out.println("Inside appconfigbean class ...uiActionName is=======" + uiActionName );
	}


	public String getUiActionName()
	{
		System.out.println("Inside appconfigbean class ...uiActionName is=======" + uiActionName );
		return this.uiActionName;
	}

	public void setHandlerClassName(String handlerClassName)
	{
		this.handlerClassName = handlerClassName;
		System.out.println("Inside appconfigbean class ...handlerClassName is=======" + handlerClassName );
	}


	public String getHandlerClassName()
	{
		System.out.println("Inside appconfigbean class  getHandlerClassName()...handlerClassName is=======" + handlerClassName );

		return this.handlerClassName;
	}

	
	public void setUiSuccessPage(String uiSuccessPage)
	{
		this.uiSuccessPage = uiSuccessPage;
	}

	public String getUiSuccessPage()
	{
		return this.uiSuccessPage;
	}
	
	
}
