package com.webapp.viewhelper;

import javax.servlet.http.HttpServletRequest;

public interface GenericViewHelper 
{
	public String execute(HttpServletRequest request, String uiActionName) throws Exception;

}
